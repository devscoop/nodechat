# NodeChat

A simple chat server built with Node.js. You can use it for your examples or for learning. Any errors, please let me know.

## Requires
#### Socket.io
#### Redis
#### FS
#### Http

## Functionality

user accesses page -> prompted for nickname/username -> nickname stores within Node.js (set/get) -> nickname/username stored within redis -> redis look up for other members -> redis look up for 10 recent messages -> emited to user

user sends message -> message added to users own page -> nickname/username retrieved from Node.js -> stored in redis -> broadcasted to all users connected -> stored in redis  
