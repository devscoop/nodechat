// create a HttpServer for the socket.io instance
var app = require('http').createServer(handler)
// bind socket.io to listen to the server
var io = require('socket.io').listen(app)
// file system
var fs = require('fs')
// redis persistence 
var redis = require('redis').createClient()
// listen to port 8080
app.listen(8080);

// function to handle initial url request
function handler (req, res) {
  fs.readFile(__dirname + '/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500); // error!
      return res.end('Error loading index.html');
    }
		// A-OK!
    res.writeHead(200);
    res.end(data);
  });
}
// disabled - will clear all redis data
//redis.flushall()

// store messages to redis
var redisStoreMessages = function(username,message) {
	redis.lpush("messages", username+": "+message, function(err, reply){
		redis.ltrim("messages",0,10); // store only the last 10 messages
	});
}

// add members to redis
var redisAddMembers = function(username) {
	redis.sadd("members",username);
}

// echo messages from redis
// socket - socket.io instance
// limit - the number of posts to display
var redisEcho = function(socket,limit) {
	// show messages in range
	redis.lrange('messages',0,limit,function(err,messages){
		messages = messages.reverse();
		messages.forEach(function(message){
				socket.emit('new-message',message);
		});
	});
}

// remove debug messages - uncomment to see
io.set('log level', 1);


io.sockets.on('connection', function(socket) {
		
		// add to members list
		socket.on('new-user', function(data) {
				// set to socket variable for later retrieval
				socket.set('nickname',data['username']);
				// broadcast the new user alert 
				socket.broadcast.emit("new-user",data['username']);
				redisAddMembers(data['username']);
				redis.smembers('members',function(err,names) {
						names.forEach(function(name){
								socket.emit('add-user',name); // add new usernames to read streams
						});
				});
				redisEcho(socket,-1);
				
		});
		
		// broadcast chat messages
		socket.on('new-messages',function(data){
				socket.get('nickname',function(err,name){
					redisStoreMessages(name,data['message']);
					socket.broadcast.emit('new-message',name +": "+data['message']);
					//redisEcho(socket,1);
				});
				
		});
		
		socket.on('disconnect', function(name) {
			//remove from list
			socket.get('nickname',function(err,name) {
					console.log('Disconnected '+name);
					socket.broadcast.emit('remove-user',name);
					redis.srem('members',name);
			});

		});
});


